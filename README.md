# GNOME Shell Extensions Review Tools

Tool to help GNOME Shell extension reviewers to fetch review file and installing the extension in Live environment.

## How to use

You can start the review by running `review-tool.sh` script with review ID:

```bash
./review-tool.sh REVIEW_ID
```

## License

This Software has been released under GPL version 3 License.
