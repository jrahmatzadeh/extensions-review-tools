#!/bin/bash

#
#  LICENSE:
# 
#    Copyright 2021 Javad Rahmatzadeh
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <https://www.gnu.org/licenses/>.
# 
#  @author     Javad Rahmatzadeh <j.rahmatzadeh@gmail.com>
#  @copyright  2021
#  @license    GPL version 3
#

#
# get download filename
#  
# @param $1 int review id
#
# @return string
#
function getDownloadFilename
{
    local id=$1
    echo "$id.shell-extension.zip"
}

#
# download extension from e.g.o website
#  
# @param $1 int review id
#
# @return string 0|1
#
function downloadExtension
{
    local id=$1
    local filename=$(getDownloadFilename $id)
    local url="https://extensions.gnome.org/review/download/$filename"
    wget -q $url 1.zip && echo "1" || echo "0"
}

extensionId=$1

echo "GNOME Extension Reviwer tool"

# Downloading
echo "Downloading Extension ID $extensionId ..."

if [[ $(downloadExtension $extensionId) ]];
then
    echo "[OK] Download"
else
    echo "[ERROR] Download"
    exit
fi;

# Installing
echo "Installing Extension"

filename=$(getDownloadFilename $extensionId)
gnome-extensions install --force $filename
rm $filename

# Restart X11
echo "Restarting GNOME Shell"
busctl --user call org.gnome.Shell /org/gnome/Shell org.gnome.Shell \
	Eval s 'Meta.restart("Restarting GNOME Shell to Load Extensions ...")' \
	bs true ""

# journalctl
echo "View Journalctl"
journalctl -f
